-- [SECTION] Inserting Records - CREATE
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");
INSERT INTO artists (name) VALUES ("THE 1975");
INSERT INTO artists (name) VALUES ("Cigarettes After S**");
INSERT INTO artists (name) VALUES ("Night Traveler");

INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Psy 6", "2012-1-1", 1);
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Trip", "1999-1-1", 2);
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("iliwysfyasbysuoi", "2016-1-1", 3);
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("I", "2012-1-1", 4);
INSERT INTO albums (album_title, data_released, artist_id) VALUES ("Night Traveler, Vol. 1", "2012-1-1", 5);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-Pop", 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Don't Forget Me", 449, "Indie", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Robbers", 414, "Pop", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Starry Eyes", 414, "Indie", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Kisapmata", 439, "OPM", 2);





-- [SECTION] Selecting Records - READ
-- Selecting the song_name and genre of all songs
SELECT song_name, genre FROM songs;
-- Selecting all of the songs
SELECT * FROM songs;
-- Selecting all of the songs under Pop genre
SELECT song_name FROM songs WHERE genre = "Pop";
-- Selecting album title and artist id from album table
SELECT album_title, artist_id FROM albums;
-- We can us AND and OR for mutiple expressions in the WHERE clause
SELECT song_name, length FROM songs WHERE length > 420 AND genre = "Indie";
SELECT song_name, length FROM songs WHERE length < 240 OR genre = "OPM";





-- [SECTION] Updating Records - UPDATE
-- Update the length of Gangnam Style to 240
UPDATE songs SET length = 240 WHERE song_name = "Gangnam Style";
-- Update the length of Kisapmata to 240
UPDATE songs SET length = 439 WHERE song_name = "Kisapmata";
-- Note: Removing the WHERE clause will update all rows





-- [SECTION] Deleting Records - DELETE
DELETE FROM songs WHERE genre = "OPM" AND length > 240;
-- Note: Removing the WHERE clause will delete all rows